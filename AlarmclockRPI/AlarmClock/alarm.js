log = function(str){
  var now = new Date();
  var month = (parseInt(now.getMonth()) + 1);
  if(month < 10){
    month = '0' + month;
  }

  var day = now.getDate();
  if(day < 10){
    day = '0' + day;
  }
  if(typeof str === "object"){
    console.log(now.getFullYear() + "-" + month + "-" + day);
    console.log(str);  
  }
  else{
    console.log(now.getFullYear() + "-" + month + "-" + day + " - " + str);
  }
}

var http = require('http');
var fileSystem = require('fs');
var path = require('path');
var querystring = require('querystring');
var util  = require('util');
var spawn = require('child_process').spawn;
var exec = require('child_process').exec, child;

var jukebox;

var weekdays = {
  sun: 0,
  mon: 1,
  tue: 2,
  wed: 3,
  thu: 4,
  fri: 5,
  sat: 6
};

var data = {
  sun: {hour: null, min: null, timer: null},
  mon: {hour: null, min: null, timer: null},
  tue: {hour: null, min: null, timer: null},
  wed: {hour: null, min: null, timer: null},
  thu: {hour: null, min: null, timer: null},
  fri: {hour: null, min: null, timer: null},
  sat: {hour: null, min: null, timer: null},
  next: {hour: null, min: null, timer: null},
  count: {hour: null, min: null, timer: null},
  error: ""
};


http.createServer(function (req, res) {
  var fullBody = '';
  var params;

  if(req.method == 'POST'){
    req.on('data', function(chunk){
      fullBody += chunk.toString();
    });
    req.on('end', function() {
      params = querystring.parse(fullBody);
      log(params);
      setTimers(params, res);
      log(data);
    });
  }
  else{
    genResponse(res);
  }
  

}).listen(1337, 'localhost');
log('Admin on http://127.0.0.1:1337/');

http.createServer(function (req, res) {
  //TODO snooze

  if(jukebox){
    jukebox.kill();
    jukebox = null;
  }
  res.writeHead(200, {'Content-Type': 'text/plain'});
      res.end('Alarm killed!\n');
  
}).listen(1338, 'localhost');
log('Stop music on: http://127.0.0.1:1338/');

function setAlarm(day, millies, callback){
  log("Setting alarm");
  clearTimeout(data[day].timer);
  clearInterval(data[day].timer);
  data[day].timer = setTimeout(function(){
    log("ALARM!!!! " + day + " -- " + data[day].hour + ":" + data[day].min);
    playAlarm();
    if(callback){
      callback(day);
    }
  }, millies);
}

function genResponse(res){
  var swig  = require('swig');
  var file = swig.renderFile('alarm.tpl', data);
  res.writeHead(200, {
      'Content-Type': 'text/html',
  });
  res.end(file);
}

function playAlarm(){
  log("PLAY ALARM!!!!");
  var status;
  var result;
  exec("curl -I  --stderr /dev/null http://www.google.com | head -1 | cut -d' ' -f2", 
      function(error, stdout, stderr){
        status = stdout.trim();
        log("network status: " + status);
        result = parseInt(status) < 400;

        if(jukebox){
          jukebox.kill();  
        }
        
        if(result){ //online playlist
          jukebox = spawn(
            './jukebox', 
            ['-u', '11124150493', '-p', 'eighty8Miles', '-l', 'Metallica']);
        }
        else{ //offline playlist
          jukebox = spawn(
            './jukebox', 
            ['-u', '11124150493', '-p', 'eighty8Miles', '-l', 'Niburta – Scream From The East']);
        }

        jukebox.stdout.on('data', function (data) {
          log('stdout: ' + data);
        });

        jukebox.stderr.on('data', function (data) {
          log('stderr: ' + data);
        });

        jukebox.on('exit', function (code) {
          log('child process exited with code ' + code);
        });
      });
}

function setTimers(params, res){
  var now = new Date();
  for(key in data){
    if(key === "error"){
      continue;
    }

    if(key === "next" && params["next-hour"].length && params["next-min"].length){ //mai extra ébresztés
      var alarmTime = new Date();
      alarmTime.setHours(params["next-hour"]);
      alarmTime.setMinutes(params["next-min"]);
      alarmTime.setSeconds(0);
      if(alarmTime > now){
        data.next.hour = params["next-hour"];
        data.next.min = params["next-min"];
        var millies = alarmTime.getTime() - now.getTime();
        setAlarm("next", millies, function(){
          data.next = {hour: null, min: null, timer: null};
        });
      }
      else{
        data.error += "Too early for today...";
      }
      
    }
    else if(key === "count" && params["count-hour"].length && params["count-min"].length){//visszaszámlálás
      data.count.hour = params["count-hour"];
      data.count.min = params["count-min"];
      var millies = parseInt(params["count-hour"]) * 60 * 60 * 1000;
      millies += parseInt(params["count-min"]) * 60 * 1000;
      setAlarm("count", millies, function(){
        data.count = {hour: null, min: null, timer: null};
      });
    }
    else if(params[key + "-hour"] && params[key + "-hour"].length && params[key + "-min"] && params[key + "-min"].length){ //heti ébresztések
      var alarmTime = new Date();
      
      var alarmDay = weekdays[key];
      var thisDay = now.getDay();
      if(alarmDay > thisDay){
        alarmTime.setDate(alarmTime.getDate() + (alarmDay - thisDay));
      }
      else if(alarmDay === thisDay){
        alarmTime.setHours(params[key + "-hour"]);
        alarmTime.setMinutes(params[key + "-min"]);
        if(now > alarmTime){ //nem ma, hanem majd jövőhéten legközelebb
          alarmTime.setDate(alarmTime.getDate() + 7);
        }
      }
      else{
        var daystoAdd = 6 - thisDay + alarmDay + 1;
        alarmTime.setDate(alarmTime.getDate() + daystoAdd);
      }

      data[key].hour = params[key + "-hour"];
      data[key].min = params[key + "-min"];

      alarmTime.setHours(params[key + "-hour"]);
      alarmTime.setMinutes(params[key + "-min"]);
      alarmTime.setSeconds(0);
      var millies = alarmTime.getTime() - now.getTime();
      setAlarm(key, millies, setWeeklyAlarmInterval);

    }
    else{ //clear alarm
      data[key] = {hour: null, min: null, timer: null};
    }
  }
  genResponse(res);
}

setWeeklyAlarmInterval = function(day){
  log("Setting alarm interval for " + day);
  data[day].timer = setInterval(function(){
    log("ALARM!!!!");
    //playAlarm();
  }, 1000 * 3600 * 168);
};

