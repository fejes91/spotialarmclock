/*TODO
websocketen beszélünk a kliensel http://einaros.github.io/ws/
          nem kell express, egy oldal van, ami változik
          alapból zöld, fel vannak véve az ébresztések
          ha ébresztés van akkor megnyitásra rögtön snoozolja az ébresztőt, gombbal ki lehet kapcsolni teljesen
snooze esetleg visszaszámol

mindig kiírja, hogy mikor lesz a következő ébresztés és vehetünk fel következőt, vagy visszazámlálót
lehetne "következőből" több
konfig mentése fájlba / egyszerű DB-be
          külön konfig oldalon a spotify user, spotify playlist kezelés (esetleg appból lehetne offline-ba tenni playlistet, de ez exrém imbaság)

POST után window.location.href = window.location.href (POST-REDIRECT-GET)
*/

var config = require("./config.js");
//var host = '192.168.0.16';  //TODO config fájl
var jukebox;


var log4js = require('log4js'); 
log4js.loadAppender('file');
log4js.addAppender(log4js.appenders.file('log/alarm.log'), 'alarm');
var logger = log4js.getLogger('alarm');
logger.setLevel(config.loggerLevel);

var http = require('http');
var querystring = require('querystring');
var spawn = require('child_process').spawn;
var exec = require('child_process').exec, child;
var express = require('express');
var app = express();
var swig  = require('swig');


var STATUS = {READY: 0, SNOOZE: 1, ALARM: 2, PLAY: 3, TEST: 4};
var CURRENT_STATUS = STATUS.READY;

var weekdays = {
  sun: 0,
  mon: 1,
  tue: 2,
  wed: 3,
  thu: 4,
  fri: 5,
  sat: 6
};

var data = {
  alarms: {
    sun: {hour: null, min: null, timer: null},
    mon: {hour: null, min: null, timer: null},
    tue: {hour: null, min: null, timer: null},
    wed: {hour: null, min: null, timer: null},
    thu: {hour: null, min: null, timer: null},
    fri: {hour: null, min: null, timer: null},
    sat: {hour: null, min: null, timer: null},
    next: {hour: null, min: null, timer: null},
    count: {alarmDate: null, hour: null, min: null, timer: null}
  },
  settings: {
    spUser: config.spUser,
    spPass: config.spPass,
    spOnline: 'Alarmclock online',
    spOffline: 'Alarmclock offline',
    playTime: 30
  },
  error: ""
};


var snooze, autoTurnOff;
var snoozeTime = config.snoozeTime;



app.post('/', function (req, res) {
  var fullBody = '';
  var params;
  if(req.method == 'POST'){
    req.on('data', function(chunk){
      fullBody += chunk.toString();
    });
    req.on('end', function() {
      params = querystring.parse(fullBody);
      logger.debug(params);

      if(params["alarm"]){
        setTimers(params, res);
      }
      else if(params["settings"]){
        saveSettings(params, res);
      }
      else if(params["kill"]){
        killAlarm(res);
      }
      else if(params["play"]){
        playAlarm();
        CURRENT_STATUS = STATUS.PLAY;
        genResponse(res);
      }
      else if(params["test"]){
        if(jukebox){
          killAlarm();
        }
        else{
          playAlarm();
        }
        CURRENT_STATUS = STATUS.TEST;
        genResponse(res);
      }
      
    });
  }
});

app.get('/', function (req, res) {
  genResponse(res);
});

var server = app.listen(config.port, function () {

  app.set('domain', config.host);
  var host = server.address().address
  var port = server.address().port

  logger.debug('Example app listening at http://%s:%s', host, port)

});



function genResponse(res){
  console.log("gen response for status: " + CURRENT_STATUS);
  var file = null;
  if(CURRENT_STATUS === STATUS.READY){
    if(data.alarms.count.alarmDate){
      now = new Date();
      milliesToAlarm = data.alarms.count.alarmDate - now.getTime();
      data.alarms.count.min = parseInt((milliesToAlarm / (1000*60)) % 60);
      data.alarms.count.hour = parseInt((milliesToAlarm / (1000*60*60)) % 24);
    }

    file = swig.renderFile('alarm.tpl', data);
    }
  else if(CURRENT_STATUS === STATUS.ALARM || CURRENT_STATUS === STATUS.SNOOZE){
    CURRENT_STATUS = STATUS.SNOOZE;
    if(jukebox){
      jukebox.kill();
      jukebox = null;
      setSnooze();
    }
    file = swig.renderFile('snooze.tpl', {});
  }
  else if(CURRENT_STATUS === STATUS.PLAY){
    file = swig.renderFile('snooze.tpl', {});
  }
  else if(CURRENT_STATUS === STATUS.TEST){
    file = swig.renderFile('alarm.tpl', data);
  }
  res.writeHead(200, {
      'Content-Type': 'text/html',
  });
  res.end(file);
}

function killAlarm(res){
  if(jukebox){
      jukebox.kill();
      jukebox = null;
    }
    clearTimeout(snooze);
    CURRENT_STATUS = STATUS.READY;

    if(res){
      genResponse(res);  
    }    
}

function playAlarm(){
  logger.debug("PLAY ALARM!!!!");
  var status;
  var result;
  exec("curl -I  --stderr /dev/null http://www.google.com | head -1 | cut -d' ' -f2", 
      function(error, stdout, stderr){
        status = stdout.trim();
        logger.debug("network status: " + status);
        result = parseInt(status) < 400;

        if(jukebox){
          jukebox.kill();  //TODO kilépő signal lehetne itt
        }

        CURRENT_STATUS = STATUS.ALARM;
        if(result){ //online playlist
          jukebox = spawn(
            './jukebox', 
            ['-u', data.settings.spUser, '-p', data.settings.spPass, '-l', data.settings.spOnline]);
        }
        else{ //offline playlist
          jukebox = spawn(
            './jukebox', 
            ['-u', data.settings.spUser, '-p', data.settings.spPass, '-l', data.settings.spOffline]);
        }

        autoTurnOff = setTimeout(function(){
          killAlarm();
        }, data.settings.playTime * 1000 * 60);

        jukebox.stdout.on('data', function (data) {
          logger.debug("STDOUT: " + data);
        });

        jukebox.stderr.on('data', function (data) {
          logger.debug('ERROR: ' + data);
        });

        jukebox.on('exit', function (code) {
          logger.debug('child process exited with code ' + code);
        });
      });
}

function setAlarm(day, millies, callback){
  logger.debug("Setting alarm");
  clearTimeout(data.alarms[day].timer);
  clearInterval(data.alarms[day].timer);
  data.alarms[day].timer = setTimeout(function(){
    logger.debug("ALARM!!!! " + day + " -- " + data.alarms[day].hour + ":" + data.alarms[day].min);
    playAlarm();
    if(callback){
      callback(day);
    }
  }, millies);
}

function setTimers(params, res){
  var now = new Date();
  for(key in data.alarms){
    if(key === "next" && params["next-hour"].length && params["next-min"].length){ //mai extra ébresztés
      var alarmTime = new Date();
      alarmTime.setHours(params["next-hour"]);
      alarmTime.setMinutes(params["next-min"]);
      alarmTime.setSeconds(0);
      if(alarmTime < now){
        alarmTime.setDate(alarmTime.getDate() + 1); //holnap
      }
      data.alarms.next.hour = params["next-hour"];
      data.alarms.next.min = params["next-min"];
      var millies = alarmTime.getTime() - now.getTime();
      setAlarm("next", millies, function(){
        data.alarms.next = {hour: null, min: null, timer: null};
      });
    }
    else if(key === "count" && params["count-hour"].length && params["count-min"].length){//visszaszámlálás
      var millies = parseInt(params["count-hour"]) * 60 * 60 * 1000;
      millies += parseInt(params["count-min"]) * 60 * 1000;
      setAlarm("count", millies, function(){
        data.alarms.count = {alarmDate: null, hour: null, min: null, timer: null};
      });

      var countDownTime = new Date();
      logger.debug("count to " + params["count-hour"] + ":" + params["count-min"]);
      logger.debug("now: " + countDownTime);
      countDownTime.setHours(countDownTime.getHours() + parseInt(params["count-hour"]));
      logger.debug("now + hours: " + countDownTime);
      countDownTime.setMinutes(countDownTime.getMinutes() + parseInt(params["count-min"]));
      logger.debug("now + mins: " + countDownTime);
      data.alarms.count.alarmDate = countDownTime.getTime();
    }
    else if(params[key + "-hour"] && params[key + "-hour"].length && params[key + "-min"] && params[key + "-min"].length){ //heti ébresztések
      var alarmTime = new Date();
      
      var alarmDay = weekdays[key];
      var thisDay = now.getDay();
      if(alarmDay > thisDay){
        alarmTime.setDate(alarmTime.getDate() + (alarmDay - thisDay));
      }
      else if(alarmDay === thisDay){
        alarmTime.setHours(params[key + "-hour"]);
        alarmTime.setMinutes(params[key + "-min"]);
        if(now > alarmTime){ //nem ma, hanem majd jövőhéten legközelebb
          alarmTime.setDate(alarmTime.getDate() + 7);
        }
      }
      else{
        var daystoAdd = 6 - thisDay + alarmDay + 1;
        alarmTime.setDate(alarmTime.getDate() + daystoAdd);
      }

      data.alarms[key].hour = params[key + "-hour"];
      data.alarms[key].min = params[key + "-min"];

      alarmTime.setHours(params[key + "-hour"]);
      alarmTime.setMinutes(params[key + "-min"]);
      alarmTime.setSeconds(0);
      var millies = alarmTime.getTime() - now.getTime();
      setAlarm(key, millies, setWeeklyAlarmInterval);

    }
    else{ //clear alarm
      if(key === "count"){
        data.alarms.count = {alarmDate: null, hour: null, min: null, timer: null};
      }
      clearTimeout(data.alarms[key].timer);
      clearInterval(data.alarms[key].timer);
      data.alarms[key] = {hour: null, min: null, timer: null};
    }
  }
  logger.debug(data.alarms);
  genResponse(res);
}



function saveSettings(params, res){
  for(key in data.settings){
    if(key === 'spPass' && params[key] === "DEFAULTPASS"){
      continue
    }
    logger.debug("key: " + key + " param: " + params[key]);
    data.settings[key] = params[key];    
  }
  logger.debug(data.settings);
  genResponse(res);
}

setWeeklyAlarmInterval = function(day){
  logger.debug("Setting alarm interval for " + day);
  data.alarms[day].timer = setInterval(function(){
    logger.debug("ALARM!!!!");
    playAlarm();
  }, 1000 * 3600 * 168);
};

function setSnooze(){
  logger.debug("Setting snooze");
  clearTimeout(snooze);
  snooze = setTimeout(function(){
    logger.debug("SNOOZE!!!! ");
    playAlarm();
  }, snoozeTime * 60 * 1000);

}


function log(str){
  var now = new Date();
  MyDateString = 
   now.getFullYear() + '-' +
    ('0' + (now.getMonth()+1)).slice(-2) + '-' + 
    ('0' + now.getDate()).slice(-2) + ' / ' + 
    ('0' + now.getHours()).slice(-2) + ':' + 
    ('0' + now.getMinutes()).slice(-2) + ':' + 
    ('0' + now.getSeconds()).slice(-2) + ':' + 
    now.getMilliseconds(); 

  if(typeof str === "object"){
    console.logger.debug(MyDateString);
    console.logger.debug(str);  
  }
  else{
    console.logger.debug(MyDateString + " - " + str);
  }
}