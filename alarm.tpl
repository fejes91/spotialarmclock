<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<style>
		body{
			margin: 0px;
			background-color: rgb(40, 88, 89);
			color: white;
		}


		.main{
			width: 320px;
			margin: 0px auto;
			
		}

		.main h1{
			text-align: center;
		}

		.row{
			text-align: center;
			margin: 10px;
		}

		input{
			font-size: 16px;
			height: 27px;
			text-align: center;
		}

		.alarm input[type='submit'], .settings input[type='submit']{
			margin-top: 10px;
			margin-bottom: 30px;
			margin-left: 50%;
			width: 200px;
			height: 35px;
			position: relative;
			left: -100px;
			padding-left: 0px;
			text-align: center;
		}

		.alarm input{
			width: 22%;
		}

		.label{
			display: inline-block;
			text-align: left;
			width: 30%;
		}

		.label.setting{
			width: 100%;
		}

		.settings input{
			width: 100%;
			text-align: left;
			padding-left: 10px;
		}

		.label.upcoming{
			width: 35%;
		}


		.tabs {
		  position: relative;   
		  min-height: 480px; /* This part sucks */
		  clear: both;

		}
		.tab {
		  float: left;
		  width: 50%;
		}
		.tab label {
		  background: rgb(40, 88, 89);
		  padding: 10px; 
		  margin-left: -1px; 
		  position: relative;
		  left: 1px; 
		  border-bottom: 1px solid white;
		  width: 90%;
		  display: inline-block;
		  text-align: center;
		}
		.tab [type=radio] {
		  display: none;   
		}
		.content {
		  position: absolute;
		  top: 40px;
		  background-color: rgb(40, 88, 89);
		  left: 0;
		  right: 0;
		  bottom: 0;

		}
		[type=radio]:checked ~ label {
		  background: rgb(45, 110, 111);
		  
		  z-index: 2;
		}
		[type=radio]:checked ~ label ~ .content {
		  z-index: 1;
		}


	</style>
</head>
<body>
	<div class="main">
		<h1>ALARM!!!</h1>
		{{error}}

		<div class="tabs">
			<div class="tab">
				<input type="radio" id="tab-1" name="tab-group-1" checked>
       			<label for="tab-1">Alarms</label>
				<form action="/" method="POST" class="alarm content">
					<div class="row">
						<span class="label">Next: </span><input name="next-hour" placeholder="Hours"  value="{{alarms.next.hour}}" type="number"/> : <input name="next-min" placeholder="Mins" value="{{alarms.next.min}}" type="number"/>
					</div>
					<div class="row">
						<span class="label">Countdown: </span><input name="count-hour" value="{{alarms.count.hour}}" placeholder="Hours" type="number"/> : <input name="count-min" placeholder="Mins" value="{{alarms.count.min}}" type="number"/>
					</div>
					<br>
					
					<div class="row">
						<span class="label">Monday: </span><input name="mon-hour" placeholder="Hours" value="{{alarms.mon.hour}}" type="number"/> : <input name="mon-min" placeholder="Mins" value="{{alarms.mon.min}}" type="number"/>
					</div>

					<div class="row">
						<span class="label">Tuesday: </span><input name="tue-hour" placeholder="Hours" value="{{alarms.tue.hour}}" type="number"/> : <input name="tue-min" placeholder="Mins" value="{{alarms.tue.min}}" type="number"/>
					</div>

					<div class="row">
						<span class="label">Wednesday: </span><input name="wed-hour" placeholder="Hours" value="{{alarms.wed.hour}}" type="number"/> : <input name="wed-min" placeholder="Mins" value="{{alarms.wed.min}}" type="number"/>
					</div>

					<div class="row">
						<span class="label">Thursday: </span><input name="thu-hour" placeholder="Hours" value="{{alarms.thu.hour}}" type="number"/> : <input name="thu-min" placeholder="Mins" value="{{alarms.thu.min}}" type="number"/>
					</div>

					<div class="row">
						<span class="label">Friday: </span><input name="fri-hour" placeholder="Hours" value="{{alarms.fri.hour}}" type="number"/> : <input name="fri-min" placeholder="Mins" value="{{alarms.fri.min}}" type="number"/><br/>
					</div>

					<div class="row">
						<span class="label">Saturday: </span><input name="sat-hour" placeholder="Hours" value="{{alarms.sat.hour}}" type="number"/> : <input name="sat-min" placeholder="Mins" value="{{alarms.sat.min}}" type="number"/>
					</div>

					<div class="row">
						<span class="label">Sunday: </span><input name="sun-hour" placeholder="Hours" value="{{alarms.sun.hour}}" type="number"/> : <input name="sun-min" placeholder="Mins" value="{{alarms.sun.min}}" type="number"/>
					</div>
					
					<input type="submit" name="alarm" value="Save"/>
				</form>
			</div>
			
			<div class="tab">
				<input type="radio" id="tab-2" name="tab-group-1">
       			<label for="tab-2">Settings</label>
				<form action="/" method="POST" class="settings content">
					<div class="row">
						<span class="label setting">Spotify user: </span><input name="spUser" value="{{settings.spUser}}" type="text"/>
					</div>
					<div class="row">
						<span class="label setting">Spotify pass: </span><input name="spPass" value="{{settings.spPass}}" type="password"/>
					</div>

					<div class="row">
						<span class="label setting">Online playlist: </span><input name="spOnline" value="{{settings.spOnline}}" type="text"/>
					</div>
					<div class="row">
						<span class="label setting">Offline playlist: </span><input name="spOffline" value="{{settings.spOffline}}" type="text"/>
					</div>
					<div class="row">
						<span class="label setting">Alarm playtime (mins): </span><input name="playTime" value="{{settings.playTime}}" type="number"/>
					</div>
					<input type="submit" name="test" value="Play/Stop test"/>
					<input type="submit" name="settings" value="Save"/>
				</form>
			</div>
		</div>
	</div>
</body>
</html>