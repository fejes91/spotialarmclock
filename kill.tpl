<!--h1>{{ pagename|title }}</h1>
<ul>
{% for author in authors %}
  <li{% if loop.first %} class="first"{% endif %}>
    {{ author }}
  </li>
{% endfor %}
</ul-->

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<style>
		body{
			margin: 0px;
			color: white;
		}

		body a{
			color: white;
		}

		.main{
			width: 320px;
			height: 480px;
			margin: 0px auto;
			background-color: rgb(40, 88, 89);
			
		}

		.main h1{
			text-align: center;
		}
	</style>
</head>
<body>
	<div class="main">
		<h1>Alarm killed!</h1>
		{{error}}
	</div>
</body>
</html>