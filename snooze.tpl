<!--h1>{{ pagename|title }}</h1>
<ul>
{% for author in authors %}
  <li{% if loop.first %} class="first"{% endif %}>
    {{ author }}
  </li>
{% endfor %}
</ul-->

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<style>
		body{
			margin: 0px;
			color: white;
			background-color: rgb(255, 174, 0);
			text-align: center;
		}

		body a{
			color: white;
			text-decoration: none;
		}

		input{
			border: 0;
			margin: 10px 0px;
			color: white;
			width: 90%;
			font-size: 32px;
			padding: 30px;
			display: inline-block;
		}

		input[name="kill"]{
			background-color: rgb(236, 91, 15);
		}

		input[name="play"]{
			background-color: rgb(40, 88, 89);
		}

		.main{
			width: 320px;
			margin: 0px auto;
			
		}

		.main h1{
			text-align: center;
		}
	</style>
</head>
<body>
	<div class="main">
		<h1>Alarm snoozed!</h1>
		<form action="/" method="POST" class="snooze">
			<input type="submit" name="kill" value="Kill it with fire!!!">
			<input type="submit" name="play" value="Play it baby!!!">
		</form>
	</div>
</body>
</html>